import time
from urllib.parse import urljoin
import json
import requests
import bs4
import base64
import requests.utils
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.common.exceptions

def get_url(well):
    id = requests.utils.quote(base64.b64encode(str.encode(well)))
    return f"https://www.ecan.govt.nz/data/well-search/welldetails/{id}/{id}"

def main():
    driver = webdriver.Firefox()
    driver.implicitly_wait(5)

    with open('src/assets/Community_Drinking_Water_Supply_Points.json') as json_file:
        data = json.load(json_file)
        features = data["features"]
        for feature in features:

            properties = feature["properties"]
            well_no = properties["WELL_NO"]
            lon, lat = feature["geometry"]["coordinates"]
            print(f"{well_no}...")
            url = get_url(well_no)
            driver.get(url)
            
            well_status = driver.find_element_by_xpath("(//*[contains(text(), 'Well Status')]/..)").find_elements_by_tag_name("td")[1].text
            if well_status.startswith("Active"):
                driver.find_element_by_xpath("//a[@data-tab='waterSamples']").click()
                text = driver.find_element_by_id("wellSamples").text
                if text == "No water sample results are available":
                    print(f"{well_no}: {text}")
                    continue
                time.sleep(0.5)
                try:
                    link = driver.find_element_by_xpath("//*[contains(text(), 'Export data for all samples')]").get_attribute("href")
                    r = requests.get(link)
                    open(f"src/assets/water_info/{well_no.replace('/', '-')}.csv", "w").write(r.text)
                    print(f"{well_no}: done")
                except selenium.common.exceptions.NoSuchElementException as e:
                    print(e)
                    print(f"{well_no}: No water sample results are available")
                # print(link)
                
            else:
                print(f"skipping, well_status: {well_status}")


if __name__ == "__main__":
    main()