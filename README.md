# GovHack2021 oh-well! Water Quality Visualiser for Christchurch

This project is an entry for the GovHack2021 event. 
A web applciation used as a tool for visualising water quality in Christchurch

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
