import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import 'leaflet/dist/leaflet.css';
import Multiselect from 'vue-multiselect'

// register globally
Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.component('multiselect', Multiselect);
//Components
import Feature from "./components/Feature";

//Routes
const routes = [
  { path: '/', component: Feature }
]

//Create Router
const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
