export class LocalApi {
    response;

     colorGenerator(expression) {
         switch (expression) {
             case 1:
                 return "#dd776e"
             case 2:
                 return "#e2886c"
             case 3:
                 return "#e79a69"
             case 4:
                 return "#ecac67"
             case 5:
                 return "#f5ce62"
             case 6:
                 return "#d4c86a"
             case 7:
                 return "#b0be6e"
             case 8:
                 return "#94bd77"
             case 9:
                 return "#73b87e"
             case 10:
                 return "#57bb8a"
             default:
                 return "#7a52aa"
         }
    }

    async getWell(wellId) {
        let well = wellId.split('/')
        let url = 'https://www.ecan.govt.nz/data/well-search/wellsummary/' + well[0] + '_' + well[1]
        console.log(url)
        let response = await fetch(url, {
            method: 'GET',
            mode: 'no-cors',
            // headers: {
            //     'Content-Type': 'text/html'
            // }
        })
        return response

    }
}
