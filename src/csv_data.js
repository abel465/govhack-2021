const jsons = []
import json0 from "@/assets/water_info_json/M35-2790.json"
jsons.push(json0)
import json1 from "@/assets/water_info_json/M36-3922.json"
jsons.push(json1)
import json2 from "@/assets/water_info_json/M36-1225.json"
jsons.push(json2)
import json3 from "@/assets/water_info_json/M35-2325.json"
jsons.push(json3)
import json4 from "@/assets/water_info_json/L36-1313.json"
jsons.push(json4)
import json5 from "@/assets/water_info_json/M36-2857.json"
jsons.push(json5)
import json6 from "@/assets/water_info_json/M35-3086.json"
jsons.push(json6)
import json7 from "@/assets/water_info_json/M35-4133.json"
jsons.push(json7)
import json8 from "@/assets/water_info_json/M35-0252.json"
jsons.push(json8)
import json9 from "@/assets/water_info_json/M35-9021.json"
jsons.push(json9)
import json10 from "@/assets/water_info_json/J38-0917.json"
jsons.push(json10)
import json11 from "@/assets/water_info_json/M36-1195.json"
jsons.push(json11)
import json12 from "@/assets/water_info_json/M35-1606.json"
jsons.push(json12)
import json13 from "@/assets/water_info_json/M35-3529.json"
jsons.push(json13)
import json14 from "@/assets/water_info_json/M36-7533.json"
jsons.push(json14)
import json15 from "@/assets/water_info_json/M35-6977.json"
jsons.push(json15)
import json16 from "@/assets/water_info_json/M34-0340.json"
jsons.push(json16)
import json17 from "@/assets/water_info_json/M35-1944.json"
jsons.push(json17)
import json18 from "@/assets/water_info_json/K37-0425.json"
jsons.push(json18)
import json19 from "@/assets/water_info_json/J41-0003.json"
jsons.push(json19)
import json20 from "@/assets/water_info_json/M35-0172.json"
jsons.push(json20)
import json21 from "@/assets/water_info_json/M36-2828.json"
jsons.push(json21)
import json22 from "@/assets/water_info_json/M35-1475.json"
jsons.push(json22)
import json23 from "@/assets/water_info_json/L35-0850.json"
jsons.push(json23)
import json24 from "@/assets/water_info_json/M35-2273.json"
jsons.push(json24)
import json25 from "@/assets/water_info_json/M35-1945.json"
jsons.push(json25)
import json26 from "@/assets/water_info_json/M35-10908.json"
jsons.push(json26)
import json27 from "@/assets/water_info_json/BX24-0624.json"
jsons.push(json27)
import json28 from "@/assets/water_info_json/P30-0001.json"
jsons.push(json28)
import json29 from "@/assets/water_info_json/M36-2560.json"
jsons.push(json29)
import json30 from "@/assets/water_info_json/M34-5707.json"
jsons.push(json30)
import json31 from "@/assets/water_info_json/M35-2789.json"
jsons.push(json31)
import json32 from "@/assets/water_info_json/M35-0217.json"
jsons.push(json32)
import json33 from "@/assets/water_info_json/M36-0423.json"
jsons.push(json33)
import json34 from "@/assets/water_info_json/M35-0834.json"
jsons.push(json34)
import json35 from "@/assets/water_info_json/M36-4053.json"
jsons.push(json35)
import json36 from "@/assets/water_info_json/M35-5135.json"
jsons.push(json36)
import json37 from "@/assets/water_info_json/M37-0106.json"
jsons.push(json37)
import json38 from "@/assets/water_info_json/K37-1703.json"
jsons.push(json38)
import json39 from "@/assets/water_info_json/K37-0671.json"
jsons.push(json39)
import json40 from "@/assets/water_info_json/M35-2243.json"
jsons.push(json40)
import json41 from "@/assets/water_info_json/L37-1558.json"
jsons.push(json41)
import json42 from "@/assets/water_info_json/M36-7504.json"
jsons.push(json42)
import json43 from "@/assets/water_info_json/M36-4795.json"
jsons.push(json43)
import json44 from "@/assets/water_info_json/J39-0135.json"
jsons.push(json44)
import json45 from "@/assets/water_info_json/M36-1965.json"
jsons.push(json45)
import json46 from "@/assets/water_info_json/M35-2554.json"
jsons.push(json46)
import json47 from "@/assets/water_info_json/M36-4699.json"
jsons.push(json47)
import json48 from "@/assets/water_info_json/M36-1597.json"
jsons.push(json48)
import json49 from "@/assets/water_info_json/M35-5875.json"
jsons.push(json49)
import json50 from "@/assets/water_info_json/M35-10794.json"
jsons.push(json50)
import json51 from "@/assets/water_info_json/M35-2555.json"
jsons.push(json51)
import json52 from "@/assets/water_info_json/M35-0833.json"
jsons.push(json52)
import json53 from "@/assets/water_info_json/J40-0250.json"
jsons.push(json53)
import json54 from "@/assets/water_info_json/M36-4700.json"
jsons.push(json54)
import json55 from "@/assets/water_info_json/M35-2556.json"
jsons.push(json55)
import json56 from "@/assets/water_info_json/M36-20671.json"
jsons.push(json56)
import json57 from "@/assets/water_info_json/M35-17757.json"
jsons.push(json57)
import json58 from "@/assets/water_info_json/M35-18638.json"
jsons.push(json58)
import json59 from "@/assets/water_info_json/M36-1055.json"
jsons.push(json59)
import json60 from "@/assets/water_info_json/M35-2270.json"
jsons.push(json60)
import json61 from "@/assets/water_info_json/M36-0985.json"
jsons.push(json61)
import json62 from "@/assets/water_info_json/M35-9566.json"
jsons.push(json62)
import json63 from "@/assets/water_info_json/M35-10795.json"
jsons.push(json63)
import json64 from "@/assets/water_info_json/M35-11908.json"
jsons.push(json64)
import json65 from "@/assets/water_info_json/M35-0474.json"
jsons.push(json65)
import json66 from "@/assets/water_info_json/K35-0034.json"
jsons.push(json66)
import json67 from "@/assets/water_info_json/M36-1197.json"
jsons.push(json67)
import json68 from "@/assets/water_info_json/M36-1210.json"
jsons.push(json68)
import json69 from "@/assets/water_info_json/M35-18017.json"
jsons.push(json69)
import json70 from "@/assets/water_info_json/L37-0545.json"
jsons.push(json70)
import json71 from "@/assets/water_info_json/N36-0048.json"
jsons.push(json71)
import json72 from "@/assets/water_info_json/M35-2609.json"
jsons.push(json72)
import json73 from "@/assets/water_info_json/M36-1030.json"
jsons.push(json73)
import json74 from "@/assets/water_info_json/M35-11910.json"
jsons.push(json74)
import json75 from "@/assets/water_info_json/O32-0018.json"
jsons.push(json75)
import json76 from "@/assets/water_info_json/M35-2152.json"
jsons.push(json76)
import json77 from "@/assets/water_info_json/J38-0251.json"
jsons.push(json77)
import json78 from "@/assets/water_info_json/M36-5377.json"
jsons.push(json78)
import json79 from "@/assets/water_info_json/M35-17636.json"
jsons.push(json79)
import json80 from "@/assets/water_info_json/BX24-0965.json"
jsons.push(json80)
import json81 from "@/assets/water_info_json/M35-5251.json"
jsons.push(json81)
import json82 from "@/assets/water_info_json/M35-1852.json"
jsons.push(json82)
import json83 from "@/assets/water_info_json/L37-0273.json"
jsons.push(json83)
import json84 from "@/assets/water_info_json/M35-0181.json"
jsons.push(json84)
import json85 from "@/assets/water_info_json/BW24-0051.json"
jsons.push(json85)
import json86 from "@/assets/water_info_json/M36-0698.json"
jsons.push(json86)
import json87 from "@/assets/water_info_json/M35-0225.json"
jsons.push(json87)
import json88 from "@/assets/water_info_json/M35-9289.json"
jsons.push(json88)
import json89 from "@/assets/water_info_json/L36-0885.json"
jsons.push(json89)
import json90 from "@/assets/water_info_json/M36-1045.json"
jsons.push(json90)
import json91 from "@/assets/water_info_json/M36-1332.json"
jsons.push(json91)
import json92 from "@/assets/water_info_json/L35-0334.json"
jsons.push(json92)
import json93 from "@/assets/water_info_json/L35-0576.json"
jsons.push(json93)
import json94 from "@/assets/water_info_json/M35-0694.json"
jsons.push(json94)
import json95 from "@/assets/water_info_json/K37-1285.json"
jsons.push(json95)
import json96 from "@/assets/water_info_json/M35-0914.json"
jsons.push(json96)
import json97 from "@/assets/water_info_json/L37-1139.json"
jsons.push(json97)
import json98 from "@/assets/water_info_json/M35-2787.json"
jsons.push(json98)
import json99 from "@/assets/water_info_json/M36-7833.json"
jsons.push(json99)
import json100 from "@/assets/water_info_json/M35-7542.json"
jsons.push(json100)
import json101 from "@/assets/water_info_json/M35-18018.json"
jsons.push(json101)
import json102 from "@/assets/water_info_json/M35-2914.json"
jsons.push(json102)
import json103 from "@/assets/water_info_json/O31-0228.json"
jsons.push(json103)
import json104 from "@/assets/water_info_json/M35-3446.json"
jsons.push(json104)
import json105 from "@/assets/water_info_json/M35-1864.json"
jsons.push(json105)
import json106 from "@/assets/water_info_json/O31-0341.json"
jsons.push(json106)
import json107 from "@/assets/water_info_json/M35-5573.json"
jsons.push(json107)
import json108 from "@/assets/water_info_json/M35-6667.json"
jsons.push(json108)
import json109 from "@/assets/water_info_json/K36-0984.json"
jsons.push(json109)
import json110 from "@/assets/water_info_json/M35-9439.json"
jsons.push(json110)
import json111 from "@/assets/water_info_json/M35-2873.json"
jsons.push(json111)
import json112 from "@/assets/water_info_json/M35-8542.json"
jsons.push(json112)
import json113 from "@/assets/water_info_json/I37-0009.json"
jsons.push(json113)
import json114 from "@/assets/water_info_json/M35-5585.json"
jsons.push(json114)
import json115 from "@/assets/water_info_json/M35-2587.json"
jsons.push(json115)
import json116 from "@/assets/water_info_json/J39-0889.json"
jsons.push(json116)
import json117 from "@/assets/water_info_json/K37-0356.json"
jsons.push(json117)
import json118 from "@/assets/water_info_json/J38-0192.json"
jsons.push(json118)
import json119 from "@/assets/water_info_json/M35-11199.json"
jsons.push(json119)
import json120 from "@/assets/water_info_json/M36-1356.json"
jsons.push(json120)
import json121 from "@/assets/water_info_json/M35-7180.json"
jsons.push(json121)
import json122 from "@/assets/water_info_json/M35-10632.json"
jsons.push(json122)
import json123 from "@/assets/water_info_json/M35-3673.json"
jsons.push(json123)
import json124 from "@/assets/water_info_json/M36-1862.json"
jsons.push(json124)
import json125 from "@/assets/water_info_json/M35-0499.json"
jsons.push(json125)
import json126 from "@/assets/water_info_json/M35-0496.json"
jsons.push(json126)
import json127 from "@/assets/water_info_json/M35-8212.json"
jsons.push(json127)
import json128 from "@/assets/water_info_json/N32-0108.json"
jsons.push(json128)
import json129 from "@/assets/water_info_json/M35-0249.json"
jsons.push(json129)
import json130 from "@/assets/water_info_json/M35-1870.json"
jsons.push(json130)
import json131 from "@/assets/water_info_json/M35-2805.json"
jsons.push(json131)
import json132 from "@/assets/water_info_json/M35-9594.json"
jsons.push(json132)
import json133 from "@/assets/water_info_json/M35-5609.json"
jsons.push(json133)
import json134 from "@/assets/water_info_json/M35-3547.json"
jsons.push(json134)
import json135 from "@/assets/water_info_json/M35-1554.json"
jsons.push(json135)
import json136 from "@/assets/water_info_json/N32-0115.json"
jsons.push(json136)
import json137 from "@/assets/water_info_json/K37-1284.json"
jsons.push(json137)
import json138 from "@/assets/water_info_json/M36-0530.json"
jsons.push(json138)
import json139 from "@/assets/water_info_json/BV24-0105.json"
jsons.push(json139)
import json140 from "@/assets/water_info_json/I40-0020.json"
jsons.push(json140)
import json141 from "@/assets/water_info_json/M35-1653.json"
jsons.push(json141)
import json142 from "@/assets/water_info_json/H39-0021.json"
jsons.push(json142)
import json143 from "@/assets/water_info_json/M36-0967.json"
jsons.push(json143)
import json144 from "@/assets/water_info_json/M36-8178.json"
jsons.push(json144)
import json145 from "@/assets/water_info_json/M36-2298.json"
jsons.push(json145)
import json146 from "@/assets/water_info_json/M36-1058.json"
jsons.push(json146)
import json147 from "@/assets/water_info_json/M35-0847.json"
jsons.push(json147)
import json148 from "@/assets/water_info_json/M35-11713.json"
jsons.push(json148)
import json149 from "@/assets/water_info_json/J40-0011.json"
jsons.push(json149)
import json150 from "@/assets/water_info_json/M35-2403.json"
jsons.push(json150)
import json151 from "@/assets/water_info_json/O32-0084.json"
jsons.push(json151)
import json152 from "@/assets/water_info_json/M35-6040.json"
jsons.push(json152)
import json153 from "@/assets/water_info_json/M36-2746.json"
jsons.push(json153)
import json154 from "@/assets/water_info_json/M36-2195.json"
jsons.push(json154)
import json155 from "@/assets/water_info_json/M36-0870.json"
jsons.push(json155)
import json156 from "@/assets/water_info_json/K37-2085.json"
jsons.push(json156)
import json157 from "@/assets/water_info_json/M34-5518.json"
jsons.push(json157)
import json158 from "@/assets/water_info_json/M35-2159.json"
jsons.push(json158)
import json159 from "@/assets/water_info_json/O31-0219.json"
jsons.push(json159)
import json160 from "@/assets/water_info_json/L36-0725.json"
jsons.push(json160)
import json161 from "@/assets/water_info_json/M35-1860.json"
jsons.push(json161)
import json162 from "@/assets/water_info_json/M36-20733.json"
jsons.push(json162)
import json163 from "@/assets/water_info_json/M35-18734.json"
jsons.push(json163)
import json164 from "@/assets/water_info_json/M35-0216.json"
jsons.push(json164)
import json165 from "@/assets/water_info_json/M35-6213.json"
jsons.push(json165)
import json166 from "@/assets/water_info_json/N32-0056.json"
jsons.push(json166)
import json167 from "@/assets/water_info_json/M35-1865.json"
jsons.push(json167)
import json168 from "@/assets/water_info_json/L37-1031.json"
jsons.push(json168)
import json169 from "@/assets/water_info_json/L37-0443.json"
jsons.push(json169)
import json170 from "@/assets/water_info_json/J39-0095.json"
jsons.push(json170)
import json171 from "@/assets/water_info_json/M36-7575.json"
jsons.push(json171)
import json172 from "@/assets/water_info_json/M36-2693.json"
jsons.push(json172)
import json173 from "@/assets/water_info_json/N34-0125.json"
jsons.push(json173)
import json174 from "@/assets/water_info_json/M35-5579.json"
jsons.push(json174)
import json175 from "@/assets/water_info_json/M35-0055.json"
jsons.push(json175)
import json176 from "@/assets/water_info_json/N34-0109.json"
jsons.push(json176)
import json177 from "@/assets/water_info_json/M35-8211.json"
jsons.push(json177)
import json178 from "@/assets/water_info_json/M35-11693.json"
jsons.push(json178)
import json179 from "@/assets/water_info_json/L35-0980.json"
jsons.push(json179)
import json180 from "@/assets/water_info_json/M35-5826.json"
jsons.push(json180)
import json181 from "@/assets/water_info_json/M36-4565.json"
jsons.push(json181)
import json182 from "@/assets/water_info_json/L37-1138.json"
jsons.push(json182)
import json183 from "@/assets/water_info_json/M35-10751.json"
jsons.push(json183)
import json184 from "@/assets/water_info_json/M35-2731.json"
jsons.push(json184)
import json185 from "@/assets/water_info_json/M36-2694.json"
jsons.push(json185)
import json186 from "@/assets/water_info_json/M36-3162.json"
jsons.push(json186)
import json187 from "@/assets/water_info_json/L37-0403.json"
jsons.push(json187)
import json188 from "@/assets/water_info_json/M36-1363.json"
jsons.push(json188)
import json189 from "@/assets/water_info_json/M35-2241.json"
jsons.push(json189)
import json190 from "@/assets/water_info_json/M35-1859.json"
jsons.push(json190)
import json191 from "@/assets/water_info_json/L35-0191.json"
jsons.push(json191)
import json192 from "@/assets/water_info_json/M35-1546.json"
jsons.push(json192)
import json193 from "@/assets/water_info_json/BX23-0242.json"
jsons.push(json193)
import json194 from "@/assets/water_info_json/M34-5864.json"
jsons.push(json194)
import json195 from "@/assets/water_info_json/M35-3731.json"
jsons.push(json195)
import json196 from "@/assets/water_info_json/M36-20557.json"
jsons.push(json196)
import json197 from "@/assets/water_info_json/M36-1915.json"
jsons.push(json197)
import json198 from "@/assets/water_info_json/M35-2589.json"
jsons.push(json198)
import json199 from "@/assets/water_info_json/M36-0872.json"
jsons.push(json199)
import json200 from "@/assets/water_info_json/K37-0492.json"
jsons.push(json200)
import json201 from "@/assets/water_info_json/M36-0529.json"
jsons.push(json201)
import json202 from "@/assets/water_info_json/M35-2260.json"
jsons.push(json202)
import data_json from '@/assets/Community_Drinking_Water_Supply_Points.json'

const data = data_json.features.map(feature => ({
    well_id: feature.properties.WELL_NO,
    coordinates:  [feature.geometry.coordinates[1], feature.geometry.coordinates[0]]
}))
const wells = []

for (const json of jsons) {
    if (json.length === 0) {
        continue
    }

    const months = ["Jan", "feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    const well_id = json[0]["Site ID"]
    const last_sample_date = json[0]["Collection Date"]
    const day = parseInt(last_sample_date.substring(0,2))
    const month = last_sample_date.substring(3,6)
    const year = parseInt(last_sample_date.substring(6, 11))

    wells.push({
        well_id: well_id,
        last_sample_date: new Date(year, months.indexOf(month), day),
        coordinates: data.find(e => e.well_id === well_id).coordinates
    })
}

export default wells
