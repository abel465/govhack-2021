import csv
import json
import os

water_info_csv_dir = "src/assets/water_info/"
water_info_json_dir = "src/assets/water_info_json/"

for filename in os.listdir(water_info_csv_dir):
    id, ext = filename.split(".")

    with open(f"{water_info_csv_dir}{filename}") as f:
        lines_after_2 = f.readlines()[2:]
        reader = csv.DictReader(lines_after_2)
        rows = list(reader)

    with open(f"{water_info_json_dir}{id}.json", 'w') as f:
        json.dump(rows, f, indent=4)
